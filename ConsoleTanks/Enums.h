#pragma once

enum class RotationAngle
{
	R_0		= 0,
	R_90	= 90,
	R_180	= 180,
	R_270	= 270
};