#pragma once
#include "Object.h"

class Tank : public Object
{
public:

	Tank();
	virtual ~Tank();

public:

	void Initialize() override;
};
