#pragma once
#include "Point.h"

namespace Direction
{
	Point Top	(0, 1);
	Point Down	(0, -1);
	Point Left	(-1, 0);
	Point Right	(1, 0);

	enum class DirectionType
	{
		Top,
		Down,
		Left,
		Right
	};
};