#pragma once
#include <vector>
#include "Point.h"

typedef std::vector<Point> Vertices;

class Object
{
public:
	Object();

protected:
	Vertices ObjectVertices;

public:
	Point WorldPosition;
	RotationAngle WorldAngleRotation;

public:
	Vertices GetVertices() const;

protected:
	virtual void Initialize() = 0;
};