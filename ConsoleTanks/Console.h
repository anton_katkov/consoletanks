#pragma once

#include <iostream>

#include <iostream>
#include <windows.h>

namespace Console
{
	// ����������� ������ � ������� �� �����������
	void Gotoxy(int x, int y)
	{
		COORD coord;
		coord.X = x;
		coord.Y = y;
		SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);
	}

	// ���������� ������ �� ����������� � �������
	void PrintToCoordinates(int x, int y, char ch)
	{
		Gotoxy(x, y);
		std::cout << ch;
	}
}