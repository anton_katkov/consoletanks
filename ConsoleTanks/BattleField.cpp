#include "BattleField.h"
#include "Console.h"
#include "Point.h"
#include "Consts.h"
#include <assert.h>
#include "Tank.h"
#include <algorithm>

BattleField::BattleField()
{
	Tank * tank = new Tank;

	tank->Initialize();
	tank->WorldPosition = Point(3, 2);
	tank->WorldAngleRotation = RotationAngle::R_270;

	ObjectsInGame.push_back(tank);

	PrintObjectsToConsole(true);
}

bool BattleField::IsExit()
{
	return false;
}

void BattleField::Update(double DeltaTime)
{
	PrintObjectsToConsole();
}

void BattleField::PrintObjectsToConsole(bool ForcePrint /* = false*/)
{
	std::vector<Point> CopyData;
	std::vector<Point> RealData;

	auto MakePointsForScreen = [this](std::vector<Point>& Out)
	{
		for (const auto * obj : ObjectsInGame)
		{
			auto & Vertices = obj->GetVertices();
			Point WorldPosition = obj->WorldPosition;

			Point worldPointWithRotate = WorldPosition; //ConvertObjectCoordinateToConsoleCoordinate(WorldPosition);
			//worldPointWithRotate = worldPointWithRotate.Rotate(worldPointWithRotate, obj->WorldAngleRotation);

			Out.push_back(worldPointWithRotate);

			for (auto & Vertex : Vertices)
			{
				Point NextPos(WorldPosition.X + Vertex.X, WorldPosition.Y + Vertex.Y);
				//NextPos = ConvertObjectCoordinateToConsoleCoordinate(NextPos);
				NextPos = Point::Rotate(NextPos, worldPointWithRotate, RotationAngle::R_90);
				Out.push_back(NextPos);
			}
		}
	};

	MakePointsForScreen(CopyData);

	for (const auto & obj : ObjectsInGame)
	{
		// TODO : update logic here
	}

	MakePointsForScreen(RealData);

	for (const auto & RealPoint : RealData)
	{
		Point FindPoint(RealPoint.X, RealPoint.Y);

		std::vector<Point>::iterator it = std::find_if_not(CopyData.begin(), CopyData.end(),
			[FindPoint](Point it)
			{
				return it.X == FindPoint.X && it.Y == FindPoint.Y;
			}
		);

		if (it == CopyData.end() || ForcePrint)
		{
			Console::PrintToCoordinates(FindPoint.X, FindPoint.Y, '*');
		}
	}
}

Point BattleField::ConvertObjectCoordinateToConsoleCoordinate(Point InObjectPosition)
{
	IsCorrectPosition(InObjectPosition, true);

	Point ReturnValue;

	ReturnValue.X = InObjectPosition.X;
	ReturnValue.Y = FIELD_HEIGHT_MAX - InObjectPosition.Y - 1;

	IsCorrectPosition(ReturnValue, true);

	return ReturnValue;
}

bool BattleField::IsCorrectPosition(Point InPosition, bool CheakAssertion /*= false*/)
{
	bool ReturnValue = InPosition.X >= 0 && InPosition.Y >= 0 && InPosition.X < FIELD_WIDTH_MAX && InPosition.Y < FIELD_HEIGHT_MAX;

	if (CheakAssertion)
	{
		assert(ReturnValue, "BattleField::IsCorrectPosition() -> InPosition is bad.");
	}

	return ReturnValue;
}
