#include "Point.h"
#include <math.h>

Point Point::Rotate(Point RotatePpoint, Point Center, RotationAngle angle)
{
	float NormalAngle = (float)angle;
	Point rotated_point;
	//rotated_point.X = RotatePpoint.Y * cos(NormalAngle) - RotatePpoint.X * sin(NormalAngle);
	//rotated_point.Y = RotatePpoint.Y * sin(NormalAngle) + RotatePpoint.X * cos(NormalAngle);

	//rotated_point.X = RotatePpoint.X * cos(NormalAngle) - RotatePpoint.Y * sin(NormalAngle);
	//rotated_point.Y = RotatePpoint.X * sin(NormalAngle) + RotatePpoint.Y * cos(NormalAngle);

	rotated_point.X = Center.X + (RotatePpoint.X - Center.X) * cos(NormalAngle) + (RotatePpoint.Y - Center.X) * sin(NormalAngle);
	rotated_point.Y = Center.Y + (RotatePpoint.X - Center.Y) * cos(NormalAngle) - (RotatePpoint.Y - Center.Y) * sin(NormalAngle);

	return rotated_point;
}