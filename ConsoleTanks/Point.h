#pragma once
#include "Enums.h"

class Point
{
public:
	int X;
	int Y;

public:

	Point(const int X, const int Y)
	{
		this->X = X;
		this->Y = Y;
	}

	Point() 
	{
		this->X = 0;
		this->Y = 0;
	}

public:
	static Point Rotate(Point RotatePpoint, Point Center, RotationAngle angle);
};