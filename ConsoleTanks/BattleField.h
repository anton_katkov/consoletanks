#pragma once

#include <vector>
#include "Object.h"



class BattleField
{
private:
	std::vector<Object *> ObjectsInGame;

public:

	BattleField();

	bool IsExit();

	void Update(double DeltaTime);

	void PrintObjectsToConsole(bool ForcePrint = false);

public:

	class Point ConvertObjectCoordinateToConsoleCoordinate(class Point InObjectPosition);

public:

	bool IsCorrectPosition(Point InPosition, bool FireAssertion = false);
};